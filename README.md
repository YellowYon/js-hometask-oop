# js-hometask-oop

## Ответьте на вопросы

### Откуда в объектах методы `toString` и `valueOf`, хотя вы их не добавляли?

> Ответ: методы появляются из prototype созданных объектов. Обращение идёт по цепочке obj.**proto** => Object.prototype.toString()/valueOf()

### Чем отличаются `__proto__` и `prototype`?

> Ответ: **proto** есть у всех объектов и примитивов(если вызвать прото у примитива, то примитив внутри движка преобразуется к объекту)[есть какие-то частные исключения, но я пока не гуглил]. prototype есть только у функций(не стрелочных) и классов(т.к это "сахар" над функциями). По умолчанию prototype указывает на конструктор (т.е class Dog{} => Dog.prototype = Dog.constructor).
> Легче всего объяснить взаимосвязь на примерах:

```
#1
let a = 'abc';
a.__proto__ = String.prototype

#2
function A(){};
function B(){};
A.__proto__ === B.__proto__ === Function.prototype
A.prototype!==B.prototype
Прототипы различаются т.к это свойство отдельного инстанса, они есть у конкретной функции(и по умолчанию равны конструктору)
```

### Что такое `super` в классах?

> Ответ: super это вызов функции , принадлежащей родительскому классу; Обязательно использовать вызов super в конструкторе перед this, иначе будет ошибка.

### Что такое статический метод класса, как его создать?

> Ответ: Статический метод - это метод класса, который можно вызвать без создания инстанса класса.
> Что бы создать статический метод, нужно дописать static перед методом.

## Выполните задания

- Установите зависимости `npm install`;
- Допишите функции в `tasks.js`;
- Проверяте себя при помощи тестов `npm run test`;
- Создайте Merge Request с решением.
